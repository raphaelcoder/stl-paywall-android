package eu.fiveminutes.stl.access;

import org.json.JSONObject;

import eu.fiveminutes.stl.Response;

public class CheckAccessResponse extends Response {

	private boolean authorized;

	public CheckAccessResponse(boolean isAuthorized){
		this.authorized = isAuthorized;
	}
	
	public boolean isAuthorized() {
		return authorized;
	}

	@Override
	public void fromJson(JSONObject json) {
	}

}

package eu.fiveminutes.stl.access;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class SyncronexPaymeterClient {

	private static final String URL = "https://%s/%s/%s/api/svcs/subscribers/products/byexternalid/%s?format=json&source=%s";
	private static final String CLIENT_INFO = "android";
	private static AsyncHttpClient client = new AsyncHttpClient();

	public static void get(String baseUrl, String company, String property, String userID, AsyncHttpResponseHandler responseHandler) {
		client.get(makeUrl(baseUrl, company, property, userID), responseHandler);
	}

	private static String makeUrl(String baseUrl, String company, String property, String userID) {
		return String.format(URL, baseUrl, company, property, userID, CLIENT_INFO);
	}
}

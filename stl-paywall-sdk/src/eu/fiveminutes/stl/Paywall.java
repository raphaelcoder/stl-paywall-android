package eu.fiveminutes.stl;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.loopj.android.http.JsonHttpResponseHandler;

import eu.fiveminutes.stl.access.CheckAccessResponse;
import eu.fiveminutes.stl.access.SyncronexPaymeterClient;
import eu.fiveminutes.stl.login.BloxAuthenticationClient;
import eu.fiveminutes.stl.login.LoginResponse;

public class Paywall {

	private static final String PREF_USER = "userID";
	private String urlBlox, urlSyncronex, company, property;

	private Context context;
	private PrefsHandler prefsHandler;

	public Paywall(Context context, String syncronexUrl, String company, String property, String bloxUrl, String bloxKey, String bloxSecret) {
		this.context = context;
		this.urlBlox = bloxUrl;
		this.urlSyncronex = syncronexUrl;
		this.company = company;
		this.property = property;
		this.prefsHandler = new PrefsHandler();
		BloxAuthenticationClient.setAuthorizationParams(bloxKey, bloxSecret);
	}

	public void checkAccess(ResponseListener<CheckAccessResponse> callback) {
		String userID = prefsHandler.loadUserID();
		if (userID.equals("")) {
			callback.onResponse(this, new CheckAccessResponse(false), null);
		} else {
			SyncronexPaymeterClient.get(urlSyncronex, company, property, userID, new CheckAccessResponseHandler(callback));
		}
	}

	public void login(String user, String password, ResponseListener<LoginResponse> callback) {
		BloxAuthenticationClient.post(urlBlox, user, password, new LoginResponseHandler(callback));
	}

	public void clearUser() {
		prefsHandler.clearUserID();
	}

	public boolean isUserSignedIn(){
		return !prefsHandler.loadUserID().equals("");
	}
	
	private class LoginResponseHandler extends ResponseHandler<LoginResponse> {

		public LoginResponseHandler(ResponseListener<LoginResponse> callback) {
			super(callback, LoginResponse.class);
		}

		@Override
		public void onSuccess(int statusCode, JSONObject jsonResponse) {
			LoginResponse response = null;
			if (jsonResponse != null) {
				response = new LoginResponse();
				response.fromJson(jsonResponse);
				prefsHandler.storeUserID(response.getId());
			}
			if (listener != null) {
				listener.onResponse(Paywall.this, response, null);
			}
		}
	}

	private class CheckAccessResponseHandler extends ResponseHandler<CheckAccessResponse> {

		public CheckAccessResponseHandler(ResponseListener<CheckAccessResponse> callback) {
			super(callback, CheckAccessResponse.class);
		}

		@Override
		public void onSuccess(int statusCode, JSONArray jsonResponse) {
			CheckAccessResponse response = null;
			if (jsonResponse != null && jsonResponse.length() > 0) {
				response = new CheckAccessResponse(true);
			} else {
				response = new CheckAccessResponse(false);
				clearUser();
			}
			if (listener != null) {
				listener.onResponse(Paywall.this, response, null);
			}
		}
	}

	private abstract class ResponseHandler<T> extends JsonHttpResponseHandler {
		protected ResponseListener<T> listener;

		public ResponseHandler(ResponseListener<T> callback, Class<T> responseClass) {
			this.listener = callback;
		}

		@Override
		public void onFailure(Throwable error, JSONObject errorResponse) {
			listener.onResponse(Paywall.this, null, error);
		}

		@Override
		public void onFailure(String responseBody, Throwable error) {
			listener.onResponse(Paywall.this, null, error);
		}
	}

	private class PrefsHandler {
		SharedPreferences prefs;

		public PrefsHandler() {
			prefs = PreferenceManager.getDefaultSharedPreferences(context);
		}

		public void storeUserID(String userID) {
			prefs.edit().putString(PREF_USER, userID).commit();
		}

		public String loadUserID() {
			return prefs.getString(PREF_USER, "");
		}

		public void clearUserID() {
			prefs.edit().putString(PREF_USER, "").commit();
		}
	}
}

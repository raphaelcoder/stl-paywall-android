package eu.fiveminutes.stl;


public interface ResponseListener<T> {

	public void onResponse(Paywall paywall,  T response, Throwable error);
}

package eu.fiveminutes.stl.login;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import eu.fiveminutes.stl.Response;

public class LoginResponse extends Response {

	private String id;
	private String screen_name;
	private String avatar_url;
	private String authtoken;
	private ArrayList<String> services = new ArrayList<String>();

	// only on error
	private String message;
	private String status;
	private int code;

	@Override
	public void fromJson(JSONObject json) {
		try {
			id = json.getString("id");
		} catch (JSONException e) {
		}
		try {
			screen_name = json.getString("screen_name");
		} catch (JSONException e) {
		}
		try {
			avatar_url = json.getString("avatar_url");
		} catch (JSONException e) {
		}
		try {
			authtoken = json.getString("avatar_url");
		} catch (JSONException e) {
		}
		try {
			message = json.getString("message");
		} catch (JSONException e) {
		}
		try {
			status = json.getString("status");
		} catch (JSONException e) {
		}
		try {
			code = json.getInt("code");
		} catch (JSONException e) {
		}
		try {
			JSONArray servicesJson = json.getJSONArray("services");
			for (int i = 0; i < servicesJson.length(); i++) {
				services.add(servicesJson.getString(i));
			}
		} catch (JSONException e) {
		}
	}

	public boolean isSuccess() {
		return id != null;
	}

	public String getId() {
		return id;
	}

	public String getScreen_name() {
		return screen_name;
	}

	public String getAvatar_url() {
		return avatar_url;
	}

	public String getAuthtoken() {
		return authtoken;
	}

	public String getMessage() {
		return message;
	}

	public ArrayList<String> getServices() {
		return services;
	}

	public String getStatus() {
		return status;
	}

	public int getCode() {
		return code;
	}
	

}

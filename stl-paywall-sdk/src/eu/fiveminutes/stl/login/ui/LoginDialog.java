package eu.fiveminutes.stl.login.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import eu.fiveminutes.stl.Paywall;
import eu.fiveminutes.stl.R;
import eu.fiveminutes.stl.ResponseListener;
import eu.fiveminutes.stl.login.LoginResponse;

public class LoginDialog implements TextWatcher{

	protected EditText etUserName, etPassword;
	protected Button btLogin;
	protected AlertDialog dialog;
	protected Paywall paywall;
	
	public LoginDialog(final Context context, Paywall paywall){
		this.paywall = paywall;
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setView(LayoutInflater.from(context).inflate(R.layout.dialog_login, null));
		builder.setPositiveButton(context.getString(R.string.login), null);
		builder.setNegativeButton(context.getString(R.string.cancel), null);
		dialog = builder.create();
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
	}
	
	public void show(OnClickListener onClickListener, final OnCancelListener onCancelListener){
		dialog.show();
		
		etUserName = (EditText) dialog.findViewById(R.id.etUserName);
		etUserName.addTextChangedListener(this);
		
		etPassword = (EditText) dialog.findViewById(R.id.etPassword);
		etPassword.addTextChangedListener(this);
		
		dialog.setOnCancelListener(onCancelListener);
		
		Button btCancel = dialog.getButton(Dialog.BUTTON_NEGATIVE);
		btCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		
		btLogin = dialog.getButton(Dialog.BUTTON_POSITIVE);
		btLogin.setOnClickListener(onClickListener);
		updateLoginEnabled();
	}
	
	public void setMessage(String message){
		((TextView)dialog.findViewById(R.id.tvLoginMsg)).setText(message);
	}

	public void login(ResponseListener<LoginResponse> responseListener){
		paywall.login(etUserName.getText().toString(), etPassword.getText().toString(), responseListener);
	}

	@Override
	public void afterTextChanged(Editable s) {
		updateLoginEnabled();
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	private void updateLoginEnabled() {
		if(etUserName.getText().toString().equals("") || etPassword.getText().toString().equals("")){
			btLogin.setEnabled(false);
		} else{
			btLogin.setEnabled(true);
		}
	}

	public void hideSoftKeyboard() {
		InputMethodManager imm = (InputMethodManager) dialog.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(dialog.getCurrentFocus().getWindowToken(), 0);
	}

	public void dismiss() {
		if(dialog != null){
			try {
				dialog.dismiss();
			} catch (Exception e) {}
		}
	}
}

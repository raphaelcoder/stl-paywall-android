package eu.fiveminutes.stl;

import java.lang.ref.WeakReference;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import eu.fiveminutes.stl.access.CheckAccessResponse;
import eu.fiveminutes.stl.login.LoginResponse;
import eu.fiveminutes.stl.login.ui.LoginDialog;

public abstract class DefaultCheckAccessListener implements ResponseListener<CheckAccessResponse>, OnClickListener, OnCancelListener {

	private Context context;
	private LoginDialog loginDialog;
	private ProgressDialog progressDialog;
	
	private static WeakReference<LoginDialog> ACTIVE_LOGIN_DIALOG;
	
	public DefaultCheckAccessListener(Context context) {
		this.context = context;
		
		/*
		 *  To avoid showing more then one login dialog, we need to dismiss any old login dialog 
		 *  that was shown from another instance of DefaultCheckAccessListener.
		 */
		if (ACTIVE_LOGIN_DIALOG != null && ACTIVE_LOGIN_DIALOG.get() != null) {
			ACTIVE_LOGIN_DIALOG.get().dismiss();
			ACTIVE_LOGIN_DIALOG.clear();
		}
	}

	@Override
	public void onResponse(Paywall paywall, CheckAccessResponse response, Throwable error) {
		// always hide dialogs
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
		if (loginDialog != null) {
			loginDialog.dismiss();
		}
		
		// on error
		if (error != null){
			onError(error);
			return;
		}
		
		if (response != null && response.isAuthorized()) {
			// on authorized
			showContent();
		} else {
			// on not authorized - show login dialog or ...
			hideContent();
			if (loginDialog == null) {
				loginDialog = new LoginDialog(context, paywall);
				loginDialog.show(this, this);
				ACTIVE_LOGIN_DIALOG = new WeakReference<LoginDialog>(loginDialog);
			} else {
				onUnauthorizedAfterLogin();
			}
		}
	}

	public abstract void showContent();

	public abstract void hideContent();

	public abstract void onDialogCanceled();

	public abstract void onError(Throwable error);

	public abstract void onUnauthorizedAfterLogin();

	@Override
	public void onClick(View v) {
		loginDialog.hideSoftKeyboard();
		loginDialog.login(new LoginResponseListener());
		progressDialog = new ProgressDialog(context);
		progressDialog.setMessage(context.getString(R.string.lee_please_wait_));
		progressDialog.show();
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		loginDialog.hideSoftKeyboard();
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				onDialogCanceled();
			}
		}, 50);	// wait 50ms for SoftKeyboard to hide
	}

	private class LoginResponseListener implements ResponseListener<LoginResponse> {
		@Override
		public void onResponse(Paywall paywall, LoginResponse response, Throwable error) {
			// on error - hide dialogs
			if (error != null) {
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
				if (loginDialog != null) {
					loginDialog.dismiss();
				}
				onError(error);
				return;
			}
			
			if (response != null && response.isSuccess()) {
				// on login success - continue showing dialogs...
				paywall.checkAccess(DefaultCheckAccessListener.this);
			} else {
				// on login failed - hide progress dialog and update login dialog
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
				if (response != null && loginDialog != null) {
					loginDialog.setMessage(response.getMessage());
				}
			}
		}
	}
}

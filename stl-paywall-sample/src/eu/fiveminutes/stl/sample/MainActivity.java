package eu.fiveminutes.stl.sample;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;
import eu.fiveminutes.stl.DefaultCheckAccessListener;
import eu.fiveminutes.stl.Paywall;

public class MainActivity extends FragmentActivity implements OnClickListener{
	
	//synchronex
	private String syncronexUrl = "stage.syncaccess.net", company = "lee", property = "mad";
	
	//blox
	private String bloxKey = "C56E913C8AA711E3A1A40019BB2963F4", bloxSecret = "52EBEF24BDDFF", bloxUrl = "dev4-dot-leetemplates-dot-com.bloxcms.com";
	private Paywall paywall;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		paywall = new Paywall(this, syncronexUrl, company, property, bloxUrl, bloxKey, bloxSecret);
		findViewById(R.id.buttonTest).setOnClickListener(this);
		findViewById(R.id.buttonClear).setOnClickListener(this);
	}

	private class MyCheckAccessListener extends DefaultCheckAccessListener{

		public MyCheckAccessListener(Context context) {
			super(context);
		}

		@Override
		public void showContent() {
			findViewById(R.id.textViewContent).setVisibility(View.VISIBLE);
		}

		@Override
		public void hideContent() {
			findViewById(R.id.textViewContent).setVisibility(View.GONE);
		}

		@Override
		public void onError(Throwable error) {
			MainActivity.this.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					Toast.makeText(MainActivity.this, "Error! We are very sorry...", Toast.LENGTH_SHORT).show();
				}
			});
		}

		@Override
		public void onUnauthorizedAfterLogin() {
			Toast.makeText(MainActivity.this, "You need to go to the website to link your account to a valid subscription.", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onDialogCanceled() {
		}
		
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if(id == R.id.buttonTest){
			paywall.checkAccess(new MyCheckAccessListener(this));
		} else if(id == R.id.buttonClear){
			paywall.clearUser();
			findViewById(R.id.textViewContent).setVisibility(View.GONE);
			Toast.makeText(this, "CLEARED!", Toast.LENGTH_SHORT).show();
		}
	}
	
}
